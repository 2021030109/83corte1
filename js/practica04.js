function hacerPeticion() {
    const http = new XMLHttpRequest();
    const url = "https://jsonplaceholder.typicode.com/users";
    const userId = document.getElementById("userId").value.trim();
    http.onreadystatechange = function() {
        if (this.status == 200 && this.readyState == 4) {
            let res = document.getElementById("lista");
            res.innerHTML = ""; // Limpiar la tabla antes de mostrar los nuevos resultados
            const json = JSON.parse(this.responseText);

            for (const datos of json) {
                if (datos.id == userId) {
                    res.innerHTML += '<tr> <td class="columna1">' + datos.id + '</td>'
                        + '<td class="columna2">' + datos.name + '</td>'
                        + '<td class="columna3">' + datos.username + '</td>'
                        + '<td class="columna4">' + datos.email + '</td>'
                        + '<td class="columna5">' + datos.phone + '</td>'
                        + '<td class="columna6">' + datos.website + '</td> </tr>';
                    break; // Una vez encontrado el usuario, se puede salir del bucle
                }
            }
            res.innerHTML += "</tbody>";
        }
    };

    http.open('GET', url, true);
    http.send();
}

document.getElementById("btnMostrar").addEventListener("click", function() {
    alert("Mostrando datos...");
    hacerPeticion();
});

document.getElementById("btnLimpiar").addEventListener("click", function() {
    let res = document.getElementById("lista");
    res.innerHTML = "";
});
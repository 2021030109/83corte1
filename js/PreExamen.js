async function buscarUsuario() {
    const userId = document.getElementById("userId").value;
    if (!userId) {
        alert("Por favor, ingrese un ID de usuario.");
        return;
    }
    else {
        alert("Buscando por ID...")
    }

    try {
        const response = await axios.get(`https://jsonplaceholder.typicode.com/users/${userId}`);
        const userData = response.data;

        // Rellenar los campos con los datos del usuario
        document.getElementById("userName").value = userData.name || '';
        document.getElementById("userUsername").value = userData.username || '';
        document.getElementById("userEmail").value = userData.email || '';
        document.getElementById("userStreet").value = userData.address?.street || '';
        document.getElementById("userNumber").value = userData.address?.suite || '';
        document.getElementById("userCity").value = userData.address?.city || '';
    } catch (error) {
        console.error("Error al buscar usuario:", error);
        alert("No se pudo encontrar el usuario. Por favor, verifique el ID e inténtelo de nuevo.");
    }
}
